import { EditUserService } from './services/edit-user.service';

import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  userUpdateForm: FormGroup;
  constructor(private updateService: EditUserService) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.userUpdateForm = new FormGroup({
      name: new FormControl('', []),
      email: new FormControl('', []),
      mobile: new FormControl('', []),
    });
  }

  updateUser() {
    console.log("reached")
    this.updateService.updateUser(this.userUpdateForm.value).subscribe((result) => {
      alert('User Data Updated');
    });
  }
}
