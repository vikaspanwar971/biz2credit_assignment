import { RegisterUserService } from './services/register-user.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css'],
})
export class RegisterUserComponent implements OnInit {
  userForm: FormGroup;
  constructor(private registerService: RegisterUserService) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.userForm = new FormGroup({
      name: new FormControl('', []),
      email: new FormControl('', []),
      mobile: new FormControl('', []),
      password: new FormControl('', []),
    });
  }

  createUser() {
    console.log("reached")
    this.registerService.createUser(this.userForm.value).subscribe((result) => {
      alert('User Added');
    });
  }
}
