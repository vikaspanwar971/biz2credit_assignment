import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RegisterUserService {
  constructor(private http: HttpClient) {}

  createUser(data):Observable<any>
  {
    console.log(data)
    return this.http.post('',data);
  }
  
}
