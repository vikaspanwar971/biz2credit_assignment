import { GlobalConstants } from './../global/GlobalConstants';
import { GetUserService } from './services/get-user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {
userData;
  constructor(private viewService : GetUserService) { }

  ngOnInit(): void {
  }
  getUsers()
  {
    this.viewService.getUsers(GlobalConstants.userToken).subscribe(result=>{
      this.userData = result;
    })
  }

}
