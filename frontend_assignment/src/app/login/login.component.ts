import { GlobalConstants } from './../global/GlobalConstants';
import { LoginService } from './services/login.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginForm : FormGroup
  constructor(private loginService: LoginService,private route:Router) { }
  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.loginForm = new FormGroup({
     
      email: new FormControl('', []),
    
      password: new FormControl('', []),
    });
  }
  login()
  {
    this.route.navigateByUrl('/user')
    this.loginService.login(this.loginForm.value).subscribe(result=>{
     GlobalConstants.userToken  = result;
    })
    console.log("login")
  }


}
